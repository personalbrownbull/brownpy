# -*- coding: utf-8 -*-
"""
  @author: Brownbull - Gabriel Carcamo - carcamo.gabriel@gmail.com
  logs custom library
"""
from datetime import datetime
from .files import *
from .program import *

def logPrint(logPath, msg, logLvl = 1):
  now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
  indtLvl = str("#" * logLvl)
  logMsg = "{} {} {}". format(now, indtLvl, msg)
  print(logMsg)
  with open(logPath, 'a', encoding="utf-8") as log:
    log.write(logMsg + "\n")

def logInit(path, prefix):
  today = datetime.now().strftime('%Y%m%d')
  logPath = path + "/" + prefix + "/" + today
  now = datetime.now().strftime('%H.%M.%S')
  logName = now + ".log"
  return initFilePath(logPath, logName)

def simpleLogInit(logPath, logName):
  logName = logName + ".log"
  return initFilePath(logPath, logName)

def logTimeINI(log, stage, lvl = 1):
  startTime, startStamp = getTimeAndStamp()
  logPrint(log, "{} INI: {}".format(stage, startStamp), lvl)
  return startTime, startStamp

def logTimeEND(log, stage, startTime, lvl = 1):
  endTime, endStamp = getTimeAndStamp()
  totalTime = endTime - startTime
  logPrint(log, "{} END: {}".format(stage, str(endStamp)), lvl)
  logPrint(log, "{} Total Execution Time: {}".format(stage, totalTime), lvl)
  return endTime, endStamp, totalTime