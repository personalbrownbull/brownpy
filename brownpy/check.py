# -*- coding: utf-8 -*-
"""
  @author: Brownbull - Gabriel Carcamo - carcamo.gabriel@gmail.com
"""
from .common import *
from .files import *
from .keys import *
from .logs import *
from .program import *

print("OK imports.")