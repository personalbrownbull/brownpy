# Brownbull Python Library #
This repo contains common usage libraries for a variety of projects(mainly DAta Science related).

## Installation and updating
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install Packge like below. 
Rerun this command to check for and install  updates .
```bash
pip install git+https://Brownbull@bitbucket.org/personalbrownbull/brownpy.git
```

## Setup
### Clone
### env
```shell
python -m venv env
python3 -m venv env
```
### activate
```shell
# Open folder containing project
cd path/where/this/file/is/readme.md
env\Scripts\activate.bat
```

### install
```shell
pip install -r requirements.txt 
```

### Update requierements
```shell
pip freeze > requirements.txt
```

## execute
execute check.py and install libraries until it work.
```python
python check.py
```

### Thanks to
Mike Huls for tutorial in [how to make this a pip package](https://mikehuls.medium.com/create-your-custom-python-package-that-you-can-pip-install-from-your-git-repository-f90465867893)