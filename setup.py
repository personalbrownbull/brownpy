#!/usr/bin/env python3
import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name='brownpy',
    version='1.1.1',
    author='Gabriel Carcamo',
    author_email='carcamo.gabriel@gmail.com',
    description='Brownbull Python Tools',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://bitbucket.org/personalbrownbull/brownpy',
    project_urls = {
        "Bug Tracker": "https://bitbucket.org/personalbrownbull/brownpy/issues"
    },
    license='MIT',
    python_requires='>=3.6',
    packages=['brownpy'],
    install_requires=[
        'cffi>=1.15.0',
        'cryptography>=36.0.1',
        'numpy>=1.22.2',
        'pandas>=1.4.1',
        'psycopg2>=2.9.3',
        'pycparser>=2.21',
        'python-dateutil>=2.8.2',
        'pytz>=2021.3',
        'pywin32>=303',
        'PyYAML>=6.0',
        'six>=1.16.0',
        'XlsxWriter>=3.0.3',
        'xlwt>=1.3.0'
        ],
)
